$(function() {

   $('.js__membership__tabs a').click(function (e) {
        e.preventDefault();
        $('.js__membership__tabs a').removeClass('active')
        $(this).addClass('active')
        let index = $(this).index()
        $('.js_membership__content .membership__section.active').fadeOut(400,function(){
            $(this).removeClass('active')
            $('.js_membership__content .membership__section').eq(index).fadeIn(400,function(){
                $(this).addClass('active');
                setTimeout(() => {
                    medalAnimate()
                    $('.membership__section__item__image').removeClass('animated')
                }, 1500);
            })
        });
   });

   function isInView(elem){
        return elem.offset().top - $(window).scrollTop() - $(window).height() / 3 * 2  < elem.height() ;
    }


   $(window).scroll(function () {
        medalAnimate()
   });


   function medalAnimate(){
        $('.membership__section__item__image').each(function () {
            if (isInView($(this)) && !$(this).hasClass('animated')){
                $(this).addClass('animated')
            }
        });
   }

   medalAnimate()
});

